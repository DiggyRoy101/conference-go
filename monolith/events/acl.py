import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
from django.http import JsonResponse


def get_weather_data(city, state):
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = response.json()
    try:
        latitude = content[0]["lat"]
        longitutde = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitutde,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    json_response = response.json()
    try:
        temp = json_response["main"]["temp"]
        desc = json_response["weather"][0]["description"]

        return {"temp": temp, "description": desc}
    except (KeyError, IndexError):
        return None


def get_location_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}

    url = f"https://api.pexels.com/v1/search?query={city}+{state}"

    resp = requests.get(url, headers=headers)
    print(resp)
    return resp.json()["photos"][0]["url"]
